wget 'https://nginx.org/download/nginx-1.17.0.tar.gz'
tar -xzvf nginx-1.17.0.tar.gz
cd nginx-1.17.0/

#export LUAJIT_LIB=/luajit2-2.1-20200102/src
#export LUAJIT_INC=/usr/local/include/luajit-2.1

./configure --prefix=/opt/nginx \
	--with-ld-opt="-Wl,-rpath,/usr/local/lib" \
	--add-module=/ngx_devel_kit-0.3.1 \
	--add-module=/lua-nginx-module-0.10.17
	--without-lua_resty_core

make
make install
