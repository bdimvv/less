FROM debian:9 as build

ENV LUAJIT_LIB=/usr/local/lib
ENV LUAJIT_INC=/usr/local/include/luajit-2.1

COPY ./nmf.sh /
RUN apt update && apt install -y curl wget unzip gzip gcc build-essential libpcre++-dev libssl-dev libgeoip-dev libxslt1-dev zlibc zlib1g zlib1g-dev
RUN wget https://github.com/openresty/luajit2/archive/v2.1-20200102.tar.gz && wget https://github.com/vision5/ngx_devel_kit/archive/v0.3.1.tar.gz && wget https://github.com/openresty/lua-nginx-module/archive/v0.10.17.tar.gz 
RUN wget https://github.com/openresty/lua-resty-core/archive/v0.1.17.tar.gz && tar -xzvf v0.1.17.tar.gz && cd /lua-resty-core-0.1.17 && make && make install && cd .. && wget https://github.com/openresty/lua-resty-core/archive/master.zip && unzip ./master.zip && cp -r /lua-resty-core-master/lib/resty /resty/
RUN tar -xzvf v2.1-20200102.tar.gz && tar -xzf v0.3.1.tar.gz && tar -xzvf v0.10.17.tar.gz
RUN cd ./luajit2-2.1-20200102 && make && make install
RUN mkdir /nginx-1.17.0 && cp -r /ngx_devel_kit-0.3.1 /nginx-1.17.0/ngx_devel_kit-0.3.1 && cp -r /lua-nginx-module-0.10.17 /nginx-1.17.0/lua-nginx-module-0.10.17 && cp -r /luajit2-2.1-20200102 /nginx-1.17.0/luajit2-2.1-20200102
RUN cp /usr/local/bin/luajit /nginx-1.17.0/luajit2-2.1-20200102/luajit && chmod +x ./nmf.sh && ./nmf.sh 

FROM debian:10

#RUN apt -y install --no-install-recommends wget gnupg ca-certificates && wget  -O - https://openresty.org/package/pubkey.gpg | sudo apt-key add - && codename=`grep -Po 'VERSION="0-9]+ \(\K[^)]+' /etc/os-release`

#RUN wget https://github.com/openresty/lua-resty-core/archive/v0.1.17.tar.gz && tar -xzvf v0.1.17.tar.gz && cd /lua-resty-core-0.1.17 && make && make install && cd ..
WORKDIR /opt/nginx/sbin/nginx
COPY --from=build /usr/local/lib/ /usr/local/lib/
COPY --from=build /usr/local/share/ /usr/local/share/
#COPY --from=build /lua-resty-core-0.1.17/lib/resty/core.lua /usr/local/share/luajit-2.1.0-beta3/resty/core.lua
COPY --from=build /opt/nginx/sbin/nginx .
COPY --from=build /usr/local/lib/libluajit-5.1.so.2 /lib/x86_64-linux-gnu/
COPY --from=build /usr/local/lib/libluajit-5.1.so /usr/local/lib/libluajit-5.1.so
COPY --from=build /usr/local/lib/libluajit-5.1.so.2 /usr/local/lib/libluajit-5.1.so.2
RUN mkdir /opt/nginx/logs && mkdir /opt/nginx/conf && touch /opt/nginx/logs/error.log && chmod +x nginx
CMD ["./nginx", "-g", "daemon off;"]
#https://github.com/openresty/lua-resty-core/archive/v0.1.17.tar.gz
